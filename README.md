# GOPATH Markdown files view tools

## Installation
> go get gitlab.com/go-steven/mdview

> cd mdview

> dep ensure -v

## Example
> cd example

> go run main.go

> view markdown files via: 
>> 127.0.0.1:8080/mdview/list

>> 127.0.0.1:8080/mdview/md/gitlab.com/go-steven/README.md
 